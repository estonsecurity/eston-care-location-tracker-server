package nl.estonsecurity.care.locationtracker.messages.out;

import com.google.gson.internal.LinkedTreeMap;
import nl.estonsecurity.care.locationtracker.messages.Message;
import nl.estonsecurity.care.locationtracker.messages.MessageType;
import nl.estonsecurity.care.locationtracker.models.Patient;

public class PatientMessage extends Message {

    public PatientMessage(Patient patient) {
        super(MessageType.PATIENT.getText());

        this.data = new LinkedTreeMap();
        this.data.put("patient", patient);
    }

}
