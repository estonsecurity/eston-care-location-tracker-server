package nl.estonsecurity.care.locationtracker.messages.in;

import nl.estonsecurity.care.locationtracker.messages.Message;
import nl.estonsecurity.care.locationtracker.messages.MessageType;

public class TotpMessage extends Message {

    private TotpMessage() {
        super(MessageType.TOTP.getText());
    }

    public int getTotp() {
        return Integer.parseInt(data.get("password").toString());
    }
}
