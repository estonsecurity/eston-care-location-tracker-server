package nl.estonsecurity.care.locationtracker.messages.in;

import nl.estonsecurity.care.locationtracker.messages.Message;
import nl.estonsecurity.care.locationtracker.messages.MessageType;

public class LocationMessage extends Message {

    public LocationMessage() {
        super(MessageType.LOCATION.toString());
    }

    public String getWearable() {
        return data.get("wearable").toString();
    }

    public float getLatitude() {
        return Float.parseFloat(data.get("latitude").toString());
    }

    public float getLongitude() {
        return Float.parseFloat(data.get("longitude").toString());
    }

}
