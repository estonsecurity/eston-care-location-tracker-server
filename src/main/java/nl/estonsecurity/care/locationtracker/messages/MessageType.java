package nl.estonsecurity.care.locationtracker.messages;

public enum MessageType {
    AUTHENTICATE("authenticate"),
    TOTP("totp"),
    AUTHENTICATION_SUCCESS("authentication_success"),
    AUTHENTICATION_FAILED("authentication_failed"),
    LOCATION("location"),
    PATIENTS("patients"),
    PATIENT("patient"),
    LOGOUT("logout");

    private String text;

    MessageType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public static MessageType fromString(String text) {
        for (MessageType messageType : MessageType.values()) {
            if (messageType.text.equalsIgnoreCase(text)) {
                return messageType;
            }
        }

        return null;
    }
}
