package nl.estonsecurity.care.locationtracker.messages;

import com.google.gson.internal.LinkedTreeMap;
import nl.estonsecurity.care.locationtracker.LocationTrackerServer;

public class Message {
    protected int code;
    protected String type;
    protected LinkedTreeMap data;

    public Message(String type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public MessageType getMessageType() {
        return MessageType.fromString(type);
    }

    public String getType() {
        return type;
    }

    public LinkedTreeMap getData() {
        return data;
    }

    public static  <T> T parseMessage(String json, Class<T> messageClass) {
        return LocationTrackerServer.getGson().fromJson(json, messageClass);
    }

    public static Message parseMessage(String json) {
        return parseMessage(json, Message.class);
    }
}
