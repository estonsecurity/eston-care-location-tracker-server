package nl.estonsecurity.care.locationtracker.messages.in;

import nl.estonsecurity.care.locationtracker.messages.Message;
import nl.estonsecurity.care.locationtracker.messages.MessageType;

public class AuthenticationMessage extends Message {

    private AuthenticationMessage() {
        super(MessageType.AUTHENTICATE.getText());
    }

    public String getUsername() {
        return data.get("username").toString();
    }

    public String getPassword() {
        return data.get("password").toString();
    }

}
