package nl.estonsecurity.care.locationtracker.messages.out;

import com.google.gson.internal.LinkedTreeMap;
import nl.estonsecurity.care.locationtracker.messages.Message;
import nl.estonsecurity.care.locationtracker.messages.MessageType;
import nl.estonsecurity.care.locationtracker.models.Patient;

import java.util.List;

public class PatientsMessage extends Message {

    public PatientsMessage(List<Patient> patients) {
        super(MessageType.PATIENTS.getText());

        this.data = new LinkedTreeMap();
        this.data.put("patients", patients);
    }

}
