package nl.estonsecurity.care.locationtracker.http;

import com.sun.net.httpserver.HttpExchange;
import nl.estonsecurity.care.locationtracker.LocationTrackerServer;
import nl.estonsecurity.care.locationtracker.core.QueryParameter;
import nl.estonsecurity.care.locationtracker.messages.in.LocationMessage;
import nl.estonsecurity.care.locationtracker.messages.Message;
import nl.estonsecurity.care.locationtracker.messages.MessageType;
import nl.estonsecurity.care.locationtracker.messages.out.PatientMessage;
import nl.estonsecurity.care.locationtracker.models.DistanceAlarm;
import nl.estonsecurity.care.locationtracker.models.Location;
import nl.estonsecurity.care.locationtracker.models.Patient;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;

public class LocationHandler extends HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String response = "";

        switch (httpExchange.getRequestMethod()) {
            case "POST":
                String message = inputStreamToString(httpExchange.getRequestBody());
                DistanceAlarm distanceAlarm;
                Patient patient;

                try {
                    if (Message.parseMessage(message).getMessageType() != MessageType.LOCATION) {
                        throw new Exception();
                    }

                    LocationMessage locationMessage = Message.parseMessage(message, LocationMessage.class);

                    // Check wearable token
                    String query = "SELECT w.id, COUNT(*) as `count` FROM wearable w WHERE w.token = ?";
                    QueryParameter[] parameters = new QueryParameter[1];
                    parameters[0] = new QueryParameter(locationMessage.getWearable());
                    ResultSet resultSet = LocationTrackerServer.getDatabase().executeQuery(query, parameters);

                    if (!resultSet.next()) {
                        throw new Exception("LocationHandler: Given wearable token '" + locationMessage.getWearable() + "' does not exist in the database");
                    }

                    // Update location
                    query = "INSERT INTO location SET wearable_id = ?, latitude = ?, longitude = ?;";
                    parameters = new QueryParameter[3];
                    parameters[0] = new QueryParameter(resultSet.getInt("id"));
                    parameters[1] = new QueryParameter(locationMessage.getLatitude());
                    parameters[2] = new QueryParameter(locationMessage.getLongitude());

                    LocationTrackerServer.getDatabase().execute(query, parameters);

                    // Push updated patient to WebSocket clients
                    query = "SELECT p.id, p.custom_id, p.max_distance, p.wearable_id, r.latitude residence_latitude, r.longitude residence_longitude, l.datetime last_update, l.latitude, l.longitude\n" +
                            "FROM patient p\n" +
                            "LEFT JOIN residence r\n" +
                            "\tON r.id = p.residence_id\n" +
                            "LEFT JOIN wearable w\n" +
                            "\tON w.id = p.wearable_id\n" +
                            "LEFT JOIN location l\n" +
                            "\tON l.wearable_id = p.wearable_id\n" +
                            "WHERE l.datetime = (\n" +
                            "\t\tSELECT MAX(datetime)\n" +
                            "\t\tFROM location\n" +
                            "\t\tWHERE wearable_id = p.wearable_id\n" +
                            "\t)\n" +
                            "\tAND w.token = ?";
                    parameters = new QueryParameter[1];
                    parameters[0] = new QueryParameter(locationMessage.getWearable());

                    resultSet = LocationTrackerServer.getDatabase().executeQuery(query, parameters);

                    if (!resultSet.next()) {
                        throw new Exception("LocationHandler: Given wearable token '" + locationMessage.getWearable() + "' is not linked to a patient");
                    }

                    distanceAlarm = new DistanceAlarm(
                            resultSet.getInt("wearable_id")
                    );
                    Location residence = new Location(
                            resultSet.getFloat("residence_latitude"),
                            resultSet.getFloat("residence_longitude")
                    );
                    Location location = new Location(
                            resultSet.getFloat("latitude"),
                            resultSet.getFloat("longitude")
                    );
                    patient = new Patient(
                            resultSet.getInt("id"),
                            resultSet.getString("custom_id"),
                            resultSet.getFloat("max_distance"),
                            residence,
                            location,
                            distanceAlarm.isOutOfRange(),
                            resultSet.getTimestamp("last_update")
                    );
                } catch (Exception e) {
                    LocationTrackerServer.getLogger().logError(e.toString());
                    httpExchange.sendResponseHeaders(400, response.length());
                    break;
                }

                boolean outOfRange = distanceAlarm.isOutOfRange();

                LocationTrackerServer.getWebSocketServer().broadcastMessage(new PatientMessage(patient));

                if (outOfRange) {
                    response = "1";
                }

                httpExchange.sendResponseHeaders(200, response.length());
                break;
            default:
                httpExchange.sendResponseHeaders(404, response.length());
                break;
        }

        OutputStream os = httpExchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
