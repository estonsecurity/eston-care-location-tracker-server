package nl.estonsecurity.care.locationtracker.http;

import com.sun.net.httpserver.HttpServer;
import nl.estonsecurity.care.locationtracker.LocationTrackerServer;
import nl.estonsecurity.care.locationtracker.core.Configuration;

import java.net.InetSocketAddress;

public class Server {
    private HttpServer httpServer;

    public Server(Configuration configuration) {
        try {
            this.httpServer = HttpServer.create(
                    new InetSocketAddress(configuration.getInt("http.port", 80)),
                    0
            );
        } catch (Exception e) {
            e.printStackTrace();
            LocationTrackerServer.shutdown();
        }
        
        this.httpServer.createContext("/location", new LocationHandler());
        this.httpServer.setExecutor(null);
    }

    public void start() {
        this.httpServer.start();
        LocationTrackerServer.getLogger().logStart("HTTP Server started on port: " + this.httpServer.getAddress().getPort());
    }

    public void stop(int i) {
        this.httpServer.stop(i);
    }

}
