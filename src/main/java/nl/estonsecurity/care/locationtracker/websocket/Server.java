package nl.estonsecurity.care.locationtracker.websocket;

import nl.estonsecurity.care.locationtracker.LocationTrackerServer;
import nl.estonsecurity.care.locationtracker.core.Configuration;
import nl.estonsecurity.care.locationtracker.core.QueryParameter;
import nl.estonsecurity.care.locationtracker.messages.in.AuthenticationMessage;
import nl.estonsecurity.care.locationtracker.messages.Message;
import nl.estonsecurity.care.locationtracker.messages.MessageType;
import nl.estonsecurity.care.locationtracker.messages.in.TotpMessage;
import nl.estonsecurity.care.locationtracker.messages.out.PatientsMessage;
import nl.estonsecurity.care.locationtracker.models.DistanceAlarm;
import nl.estonsecurity.care.locationtracker.models.Location;
import nl.estonsecurity.care.locationtracker.models.Patient;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.CustomSSLWebSocketServerFactory;
import org.java_websocket.server.DefaultSSLWebSocketServerFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Server extends org.java_websocket.server.WebSocketServer {

    private HashMap<WebSocket, Client> webSocketClients;

    public Server(Configuration configuration) {
        super(new InetSocketAddress(configuration.getInt("ws.port", 8887)));

        this.webSocketClients = new HashMap<>();
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        webSocketClients.put(conn, new Client(conn));
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        webSocketClients.remove(conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        Client client = webSocketClients.get(conn);
        Message webSocketMessage = Message.parseMessage(message);

        if (!client.isAuthenticated() && webSocketMessage.getMessageType() != MessageType.AUTHENTICATE && webSocketMessage.getMessageType() != MessageType.TOTP) {
            if (!client.isUserPassAuthenticated() && webSocketMessage.getMessageType() != MessageType.AUTHENTICATE) {
                client.sendMessage(new Message(MessageType.AUTHENTICATE.getText()));
                return;
            }

            if (client.hasTotp() && !client.isTotpAuthenticated() && webSocketMessage.getMessageType() != MessageType.TOTP) {
                client.sendMessage(new Message(MessageType.TOTP.getText()));
                return;
            }

            return;
        }

        if (webSocketMessage.getMessageType() == null) {
            LocationTrackerServer.getLogger().logError("Unknown Message type: " + webSocketMessage.getType());
            return;
        }

        MessageType responseMessageType;

        switch (webSocketMessage.getMessageType()) {
            case AUTHENTICATE:
                AuthenticationMessage authenticationMessage = Message.parseMessage(message, AuthenticationMessage.class);

                if (!client.authenticate(authenticationMessage.getUsername(), authenticationMessage.getPassword())) {
                    if (client.isUserPassAuthenticated() && !client.isTotpAuthenticated()) {
                        responseMessageType = MessageType.TOTP;
                    } else {
                        client.logout();
                        responseMessageType = MessageType.AUTHENTICATION_FAILED;
                    }
                } else {
                    responseMessageType = MessageType.AUTHENTICATION_SUCCESS;
                }

                client.sendMessage(new Message(responseMessageType.getText()));
                break;
            case TOTP:
                TotpMessage totpMessage = Message.parseMessage(message, TotpMessage.class);

                if (!client.authenticate(totpMessage.getTotp())) {
                    client.logout();
                    responseMessageType = MessageType.AUTHENTICATION_FAILED;
                } else {
                    responseMessageType = MessageType.AUTHENTICATION_SUCCESS;
                }

                client.sendMessage(new Message(responseMessageType.getText()));
                break;
            case LOGOUT:
                client.logout();
                client.sendMessage(new Message(MessageType.AUTHENTICATE.getText()));
                break;
            case PATIENTS:
                List<Patient> patientList = new ArrayList<>();

                String query = "SELECT p.id, p.custom_id, p.max_distance, p.wearable_id, r.latitude residence_latitude, r.longitude residence_longitude, l.datetime last_update, l.latitude, l.longitude\n" +
                        "FROM patient p\n" +
                        "LEFT JOIN residence r\n" +
                        "\tON r.id = p.residence_id\n" +
                        "LEFT JOIN wearable w\n" +
                        "\tON w.id = p.wearable_id\n" +
                        "LEFT JOIN location l\n" +
                        "\tON l.wearable_id = p.wearable_id\n" +
                        "WHERE l.datetime = (\n" +
                        "\tSELECT MAX(datetime)\n" +
                        "\tFROM location\n" +
                        "\tWHERE wearable_id = p.wearable_id\n" +
                        ")";

                try {
                    ResultSet resultSet = LocationTrackerServer.getDatabase().executeQuery(query, new QueryParameter[0]);

                    while (resultSet.next()) {
                        DistanceAlarm distanceAlarm = new DistanceAlarm(
                                resultSet.getInt("wearable_id")
                        );
                        Location residence = new Location(
                                resultSet.getFloat("residence_latitude"),
                                resultSet.getFloat("residence_longitude")
                        );
                        Location location = new Location(
                                resultSet.getFloat("latitude"),
                                resultSet.getFloat("longitude")
                        );
                        Patient patient = new Patient(
                                resultSet.getInt("id"),
                                resultSet.getString("custom_id"),
                                resultSet.getFloat("max_distance"),
                                residence,
                                location,
                                distanceAlarm.isOutOfRange(),
                                resultSet.getTimestamp("last_update")
                        );

                        patientList.add(patient);
                    }
                } catch (SQLException e) {
                    LocationTrackerServer.getLogger().logError(e.toString());
                }

                PatientsMessage patientsMessage = new PatientsMessage(patientList);
                client.sendMessage(patientsMessage);
                break;
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();

        if (conn != null) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onStart() {
        LocationTrackerServer.getLogger().logStart("WebSocket Server started on port: " + getPort());
    }

    public void broadcastMessage(Message message) {
        for (Map.Entry<WebSocket, Client> entry : webSocketClients.entrySet()) {
            Client client = entry.getValue();

            if (client.isAuthenticated()) {
                client.sendMessage(message);
            }
        }
    }

    public void enableSecure(Configuration configuration) {
        char[] password = configuration.getValue("wss.keystore.password", "").toCharArray();

        try {
            KeyStore keyStore = KeyStore.getInstance(configuration.getValue("wss.keystore.type", ""));
            File keyStoreFile = new File(configuration.getValue("wss.keystore.file", ""));
            keyStore.load(new FileInputStream(keyStoreFile), password);

            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            keyManagerFactory.init(keyStore, password);
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("SunX509");
            trustManagerFactory.init(keyStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), null);

            SSLEngine engine = sslContext.createSSLEngine();

            List<String> ciphers = new ArrayList<>(Arrays.asList(engine.getEnabledCipherSuites()));

            // Disable weak ciphers found by Qualys SSL Labs
            ciphers.remove("TLS_RSA_WITH_3DES_EDE_CBC_SHA"); // WEAK
            ciphers.remove("TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA"); // DH 1024 bits FS WEAK
            ciphers.remove("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA"); // WEAK
            ciphers.remove("TLS_DHE_RSA_WITH_AES_128_CBC_SHA"); // DH 1024 bits FS WEAK
            ciphers.remove("TLS_DHE_RSA_WITH_AES_128_CBC_SHA256"); // DH 1024 bits FS WEAK
            ciphers.remove("TLS_DHE_RSA_WITH_AES_128_GCM_SHA256"); // DH 1024 bits FS WEAK
            ciphers.remove("TLS_DHE_RSA_WITH_AES_256_CBC_SHA"); // DH 1024 bits FS WEAK
            ciphers.remove("TLS_DHE_RSA_WITH_AES_256_CBC_SHA256"); // DH 1024 bits FS WEAK
            ciphers.remove("TLS_DHE_RSA_WITH_AES_256_GCM_SHA384"); // DH 1024 bits FS WEAK

            // Disable unwanted protocols
            List<String> protocols = new ArrayList<>(Arrays.asList(engine.getEnabledProtocols()));
            ciphers.remove("SSLv3");

            this.setWebSocketFactory(new CustomSSLWebSocketServerFactory(sslContext, protocols.toArray(new String[]{}), ciphers.toArray(new String[]{})));
        } catch (Exception e) {
            e.printStackTrace();
            LocationTrackerServer.shutdown();
        }
    }
}
