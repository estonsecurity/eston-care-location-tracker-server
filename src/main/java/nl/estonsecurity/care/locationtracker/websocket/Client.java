package nl.estonsecurity.care.locationtracker.websocket;

import nl.estonsecurity.care.locationtracker.LocationTrackerServer;
import nl.estonsecurity.care.locationtracker.core.Authentication;
import nl.estonsecurity.care.locationtracker.messages.Message;
import org.java_websocket.WebSocket;

public class Client {

    private WebSocket webSocket;
    private String username = null;
    private boolean isUserPassAuthenticated = false;
    private boolean isTotpAuthenticated = false;

    public Client(WebSocket webSocket) {
        this.webSocket = webSocket;
    }

    public Boolean authenticate(String username, String password) {
        isUserPassAuthenticated = Authentication.checkCredentials(username, password);

        if (isUserPassAuthenticated) {
            this.username = username;
        }

        return isAuthenticated();
    }

    public Boolean authenticate(int totp) {
        isTotpAuthenticated = username != null && Authentication.checkTotp(username, totp);

        return isAuthenticated();
    }

    public Boolean hasTotp() {
        return username != null && Authentication.hasTotp(username);
    }

    public Boolean isUserPassAuthenticated() {
        return isUserPassAuthenticated;
    }

    public Boolean isTotpAuthenticated() {
        return isTotpAuthenticated;
    }

    public Boolean isAuthenticated() {
        return isUserPassAuthenticated && (!hasTotp() || isTotpAuthenticated);
    }

    public void logout() {
        username = null;
        isUserPassAuthenticated = false;
        isTotpAuthenticated = false;
    }

    public void sendMessage(Message message) {
        webSocket.send(LocationTrackerServer.getGson().toJson(message));
    }
}
