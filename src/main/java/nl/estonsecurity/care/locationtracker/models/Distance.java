package nl.estonsecurity.care.locationtracker.models;

public abstract class Distance {
    /* 6372.8 km is an approximation of the radius of the average circumference
     (i.e., the average great-elliptic or great-circle radius), where the
     boundaries are the meridian (6367.45 km) and the equator (6378.14 km) */
    public static final double EARTHRADIUSINKM = 6372.8;

    // Using the Haversine-formula as decribed on "https://rosettacode.org/wiki/Haversine_formula
    public static double getDistanceInKm (Location location1, Location location2){
        double latitude1 = location1.getLatitude();
        double latitude2 = location2.getLatitude();
        double longitude1 = location1.getLongitude();
        double longitude2 = location2.getLongitude();

        double differenceLat = Math.toRadians(latitude2 - latitude1);
        double differenceLon = Math.toRadians(longitude2 - longitude1);

        latitude1 = Math.toRadians(latitude1);
        latitude2 = Math.toRadians(latitude2);

        double a = Math.pow(Math.sin(differenceLat / 2), 2) + Math.pow(Math.sin(differenceLon /2), 2) * Math.cos(latitude1) * Math.cos(latitude2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return EARTHRADIUSINKM * c; //Distance in km
    }

    public static double getDistanceInM(Location location1, Location location2){
        return (1000d * getDistanceInKm(location1, location2));
    }
}
