package nl.estonsecurity.care.locationtracker.models;

import nl.estonsecurity.care.locationtracker.LocationTrackerServer;
import nl.estonsecurity.care.locationtracker.core.QueryParameter;
import nl.estonsecurity.care.locationtracker.core.QueryParameterTypeEnum;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DistanceAlarm {
    private int wearableId;
    private Location residenceLocation;
    private Location wearableLocation;
    private float maxDistance;

    public DistanceAlarm(int wearableId){
        this.setWearableId(wearableId);
        this.getLocationFromDatabase();
    }

    private void getLocationFromDatabase(){
        String query = "SELECT l.datetime, l.latitude, l.longitude, p.max_distance, r.latitude as residence_latitude, r.longitude as residence_longitude\n" +
                "FROM wearable w\n" +
                "INNER JOIN location l\n" +
                "\tON l.wearable_id = w.id\n" +
                "INNER JOIN patient p\n" +
                "\tON p.wearable_id = w.id\n" +
                "INNER JOIN residence r\n" +
                "\tON r.id = p.residence_id\n" +
                "WHERE w.id = ?\n" +
                "ORDER BY l.datetime DESC\n" +
                "LIMIT 1";

        QueryParameter[] queryParameters = new QueryParameter[1];
        queryParameters[0] = new QueryParameter(QueryParameterTypeEnum.INTEGER, Integer.toString(this.getWearableId()));

        try{
            ResultSet resultSet = LocationTrackerServer.getDatabase().executeQuery(query, queryParameters);

            if (!resultSet.next()){
                return;
            }
            wearableLocation = new Location(resultSet.getFloat(2),resultSet.getFloat(3));
            this.maxDistance = resultSet.getFloat(4);
            residenceLocation = new Location(resultSet.getFloat(5), resultSet.getFloat(6));
        }
        catch(SQLException e){
            LocationTrackerServer.getLogger().logError(e.toString());
        }
    }

    public Boolean isOutOfRange(){
        this.getLocationFromDatabase();
        float currentDistance = (float) Distance.getDistanceInM(residenceLocation, wearableLocation);
        return (currentDistance > this.maxDistance);
    }

    public int getWearableId() {
        return wearableId;
    }

    public void setWearableId(int wearableId) {
        this.wearableId = wearableId;
    }
}
