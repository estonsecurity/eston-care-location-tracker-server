package nl.estonsecurity.care.locationtracker.models;

import java.sql.Timestamp;

public class Patient {

    private int id;
    private String customId;
    private float maxDistance;
    private Location residence;
    private Location currentLocation;
    private boolean outOfRange;
    private Timestamp lastUpdate;

    public Patient(int id, String customId, float maxDistance, Location residence, Location currentLocation, boolean outOfRange, Timestamp lastUpdate) {
        this.id = id;
        this.customId = customId;
        this.maxDistance = maxDistance;
        this.residence = residence;
        this.currentLocation = currentLocation;
        this.outOfRange = outOfRange;
        this.lastUpdate = lastUpdate;
    }
}
