package nl.estonsecurity.care.locationtracker.core;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    private SimpleDateFormat sdf;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public Logger() {
        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        logStart("Loaded logger");
    }

    public void log(Object line) {
        Date now = new Date();
        System.out.println("[" + sdf.format(now) + "]" + line.toString());
    }

    public void logInfo(Object line) {
        log("[INFO] " + line.toString());
    }

    public void logStart(Object line) {
        log("[" + ANSI_BLUE + "LOADING" + ANSI_RESET + "] " + line.toString());
    }

    public void logShutdown(Object line) {
        log("[" + ANSI_YELLOW + "SHUTDOWN" + ANSI_RESET + "] " + line.toString());
    }

    public void logDebug(Object line) {
        log("[" + ANSI_GREEN + "DEBUG" + ANSI_RESET + "] " + line.toString());
    }

    public void logError(Object line) {
        log("[" + ANSI_RED + "ERROR" + ANSI_RESET + "] " + line.toString());
    }
}
