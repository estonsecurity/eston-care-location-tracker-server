package nl.estonsecurity.care.locationtracker.core;

import nl.estonsecurity.care.locationtracker.LocationTrackerServer;

public class QueryParameter {

    private QueryParameterTypeEnum parameterType;
    private String value;

    public QueryParameter(QueryParameterTypeEnum parameterType, String value) {
        this.parameterType = parameterType;
        this.value = value;
    }

    public QueryParameter(String value) {
        this.parameterType = QueryParameterTypeEnum.STRING;
        this.value = value;
    }

    public QueryParameter(Integer value) {
        this.parameterType = QueryParameterTypeEnum.INTEGER;
        this.value = value.toString();
    }

    public QueryParameter(Float value) {
        this.parameterType = QueryParameterTypeEnum.FLOAT;
        this.value = value.toString();
    }

    public QueryParameter(Boolean value) {
        this.parameterType = QueryParameterTypeEnum.BOOLEAN;
        this.value = value.toString();
    }

    public QueryParameterTypeEnum getParameterType() {
        return parameterType;
    }

    public String getValue() {
        return value;
    }

    public int getInt() {
        try {
            return Integer.parseInt(getValue());
        } catch (Exception e) {
            LocationTrackerServer.getLogger().logError("QueryParameter: Failed to parse value \"" + getValue() + "\" to integer");
            throw e;
        }
    }

    public float getFloat() {
        try {
            return Float.parseFloat(getValue());
        } catch (Exception e) {
            LocationTrackerServer.getLogger().logError("QueryParameter: Failed to parse value \"" + getValue() + "\" to float");
            throw e;
        }
    }

    public boolean getBoolean() {
        try {
            return Boolean.parseBoolean(getValue());
        } catch (Exception e) {
            LocationTrackerServer.getLogger().logError("QueryParameter: Failed to parse value \"" + getValue() + "\" to boolean");
            throw e;
        }
    }
}
