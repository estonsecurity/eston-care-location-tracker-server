package nl.estonsecurity.care.locationtracker.core;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import nl.estonsecurity.care.locationtracker.LocationTrackerServer;

import javax.xml.transform.Result;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {

    private HikariDataSource dataSource;

    public Database(Configuration configuration) {

        boolean dbException = false;

        try {
            HikariConfig dbConfiguration = new HikariConfig();
            dbConfiguration.setJdbcUrl("jdbc:mysql://" + configuration.getValue("db.host", "localhost") + ":" + configuration.getInt("db.port", 3306) + "/" + configuration.getValue("db.schema", ""));
            dbConfiguration.setUsername(configuration.getValue("db.username", "root"));
            dbConfiguration.setPassword(configuration.getValue("db.password", ""));

            dbConfiguration.addDataSourceProperty("verifyServerCertificate", configuration.getBoolean("db.verifyServerCertificate", true));
            dbConfiguration.addDataSourceProperty("useSSL", configuration.getBoolean("db.useSSL", true));

            dbConfiguration.setMaximumPoolSize(50);
            dbConfiguration.setMinimumIdle(10);
            dbConfiguration.addDataSourceProperty("dataSource.logger", "com.mysql.jdbc.log.StandardLogger");
            dbConfiguration.addDataSourceProperty("dataSource.logSlowQueries", "true");
            dbConfiguration.addDataSourceProperty("prepStmtCacheSize", "500");
            dbConfiguration.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            dbConfiguration.addDataSourceProperty("cachePrepStmts", "true");
            dbConfiguration.addDataSourceProperty("useServerPrepStmts", "true");
            dbConfiguration.addDataSourceProperty("rewriteBatchedStatements", "true");
            dbConfiguration.addDataSourceProperty("useUnicode","true");
            dbConfiguration.setAutoCommit(true);
            dbConfiguration.setConnectionTimeout(1000);
            dbConfiguration.setValidationTimeout(5000L);
            dbConfiguration.setLeakDetectionThreshold(20000L);
            dbConfiguration.setMaxLifetime(1800000L);
            dbConfiguration.setIdleTimeout(600000L);

            dataSource = new HikariDataSource(dbConfiguration);
        } catch (Exception e) {
            dbException = true;
            e.printStackTrace();
            LocationTrackerServer.getLogger().logError("Failed to connect to the database");
        } finally {
            if (dbException) {
                LocationTrackerServer.shutdown();
            }
        }

        LocationTrackerServer.getLogger().logStart("Loaded database connection");
    }

    public void close() {
        dataSource.close();
    }

    public HikariDataSource getDataSource() {
        return dataSource;
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    private PreparedStatement buildPreparedStatement(String query, QueryParameter[] parameters) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(query);

        for (int i = 0; i < parameters.length; i++) {
            QueryParameter parameter = parameters[i];
            int parameterIndex = i + 1;

            switch (parameter.getParameterType()) {
                case INTEGER:
                    preparedStatement.setInt(parameterIndex, parameter.getInt());
                    break;
                case FLOAT:
                    preparedStatement.setFloat(parameterIndex, parameter.getFloat());
                    break;
                case BOOLEAN:
                    preparedStatement.setBoolean(parameterIndex, parameter.getBoolean());
                    break;
                case STRING:
                    preparedStatement.setString(parameterIndex, parameter.getValue());
                    break;
            }
        }

        return preparedStatement;
    }

    public void execute(String query, QueryParameter[] parameters) throws SQLException {
        PreparedStatement preparedStatement = buildPreparedStatement(query, parameters);
        preparedStatement.execute();
        preparedStatement.getConnection().close();
    }

    public ResultSet executeQuery(String query, QueryParameter[] parameters) throws SQLException {
        PreparedStatement preparedStatement = buildPreparedStatement(query, parameters);
        ResultSet resultSet = preparedStatement.executeQuery();
        preparedStatement.getConnection().close();
        return resultSet;
    }
}
