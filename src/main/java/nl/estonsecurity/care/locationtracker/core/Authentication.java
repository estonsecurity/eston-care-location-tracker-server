package nl.estonsecurity.care.locationtracker.core;

import nl.estonsecurity.care.locationtracker.LocationTrackerServer;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Authentication {

    public static boolean checkCredentials(String username, String password) {
        String query = "SELECT username, password FROM user WHERE username = ?";
        String dbUsername = null;
        String dbPassword = null;

        QueryParameter[] queryParameters = new QueryParameter[1];
        queryParameters[0] = new QueryParameter(QueryParameterTypeEnum.STRING, username);

        try {
            ResultSet resultSet = LocationTrackerServer.getDatabase().executeQuery(query, queryParameters);

            if (!resultSet.next()) {
                return false;
            }

            dbUsername = resultSet.getString("username");
            dbPassword = resultSet.getString("password");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (dbUsername != null && BCrypt.checkpw(password, dbPassword)) {
            return true;
        }

        return false;
    }

    public static boolean hasTotp(String username) {
        String query = "SELECT COUNT(*) AS count FROM user WHERE username = ? AND totp_key IS NOT NULL";
        boolean hasTotp = false;

        QueryParameter[] queryParameters = new QueryParameter[1];
        queryParameters[0] = new QueryParameter(QueryParameterTypeEnum.STRING, username);

        try {
            ResultSet resultSet = LocationTrackerServer.getDatabase().executeQuery(query, queryParameters);

            if (resultSet.next()) {
                hasTotp = resultSet.getInt("count") > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return hasTotp;
    }

    public static boolean checkTotp(String username, int totp) {
        String query = "SELECT totp_key FROM user WHERE username = ? AND totp_key IS NOT NULL";
        boolean validCode = false;

        QueryParameter[] queryParameters = new QueryParameter[1];
        queryParameters[0] = new QueryParameter(QueryParameterTypeEnum.STRING, username);

        try {
            ResultSet resultSet = LocationTrackerServer.getDatabase().executeQuery(query, queryParameters);

            if (resultSet.next()) {
                String key = resultSet.getString("totp_key");
                validCode = LocationTrackerServer.getGAuth().authorize(key, totp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return validCode;
    }

    public static boolean checkToken(int id, String token) {
        String query = "SELECT id, token FROM WEARABLE WHERE id = ?";
        int dbId = 0;
        String dbToken = null;

        QueryParameter[] queryParameters = new QueryParameter[1];
        queryParameters[0] = new QueryParameter(QueryParameterTypeEnum.INTEGER, Integer.toString(id));

        try {
            ResultSet resultSet = LocationTrackerServer.getDatabase().executeQuery(query, queryParameters);

            if (!resultSet.next()) {
                return false;
            }

            dbId = resultSet.getInt("id");
            dbToken = resultSet.getString("token");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (dbId == id && BCrypt.checkpw(token, dbToken)) {
            return true;
        }

        return false;
    }

    public static void createWearable(String token) {
        String query = "INSERT INTO wearable (token) VALUES (?)";
        String hashedToken = BCrypt.hashpw(token, BCrypt.gensalt());

        QueryParameter[] queryParameters = new QueryParameter[1];
        queryParameters[0] = new QueryParameter(QueryParameterTypeEnum.STRING, hashedToken);

        try {
            LocationTrackerServer.getDatabase().execute(query, queryParameters);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createUser(String username, String password) {
        String query = "INSERT INTO user (username, password) VALUES (?,?)";
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());

        QueryParameter[] queryParameters = new QueryParameter[2];
        queryParameters[0] = new QueryParameter(QueryParameterTypeEnum.STRING, username);
        queryParameters[1] = new QueryParameter(QueryParameterTypeEnum.STRING, hashedPassword);

        try {
            LocationTrackerServer.getDatabase().execute(query, queryParameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
