package nl.estonsecurity.care.locationtracker.core;

import nl.estonsecurity.care.locationtracker.LocationTrackerServer;

import java.io.*;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.TreeSet;

public class Configuration {

    private final String file;
    private final Properties properties;

    public Configuration(String file) {
        this.file = file;
        this.properties = new Properties() {
            @Override
            public synchronized Enumeration<Object> keys() {
                return Collections.enumeration(new TreeSet<Object>(super.keySet()));
            }
        };

        try {
            reload();
        } catch (FileNotFoundException e) {
            LocationTrackerServer.getLogger().logError("Configuration file could not be found!");
            createDefaultFile();
            LocationTrackerServer.getLogger().logInfo("A default configuration file \"" + file + "\" is generated, please configure and restart the server");
            LocationTrackerServer.shutdown();
        }

        LocationTrackerServer.getLogger().logStart("Loaded configuration file");
    }

    /**
     * Reloads the configuration file
     *
     * @throws FileNotFoundException
     */
    public void reload() throws FileNotFoundException {
        InputStream input = null;

        try {
            File configurationFile = new File(this.file);
            input = new FileInputStream(configurationFile);
            this.properties.load(input);
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            LocationTrackerServer.getLogger().logError("[IMPORTANT] Failed to read configuration file!");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Creates default configuration file
     */
    private void createDefaultFile() {
        OutputStream output = null;

        try {
            output = new FileOutputStream(file);

            // Default server configuration
            this.properties.setProperty("debug", "false");

            // Default database configuration
            this.properties.setProperty("db.host", "localhost");
            this.properties.setProperty("db.port", "3306");
            this.properties.setProperty("db.schema", "");
            this.properties.setProperty("db.username", "root");
            this.properties.setProperty("db.password", "");

            // Default websocket configuration
            this.properties.setProperty("ws.port", "8887");
            this.properties.setProperty("wss.enabled", "false");
            this.properties.setProperty("wss.keystore.password", "");
            this.properties.setProperty("wss.keystore.type", "");
            this.properties.setProperty("wss.keystore.file", "");

            // Default http configuration
            this.properties.setProperty("http.port", "80");

            this.properties.store(output, null);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Returns the configuration value for a specific key.
     *
     * @param key          The key to find the configuration for.
     * @param defaultValue The value that will be returned when the key is not found.
     * @return String value for the key. Returns defaultValue when the key is not found.
     */
    public String getValue(String key, String defaultValue) {
        return this.properties.getProperty(key, defaultValue);
    }

    /**
     * Returns the configuration int value for a specific key.
     *
     * @param key          The key to find the configuration for.
     * @param defaultValue The value that will be returned when the key is not found.
     * @return String value for the key. Returns defaultValue when the key is not found.
     */
    public int getInt(String key, Integer defaultValue) {
        try {
            return Integer.parseInt(getValue(key, defaultValue.toString()));
        } catch (Exception e) {
            LocationTrackerServer.getLogger().logError("Configuration: Failed to parse \"" + key + "\" with value \"" + getValue(key, "") + "\" to integer");
        }

        return defaultValue;
    }

    /**
     * Returns the configuration boolean value for a specific key.
     *
     * @param key          The key to find the configuration for.
     * @param defaultValue The value that will be returned when the key is not found.
     * @return String value for the key. Returns defaultValue when the key is not found.
     */
    public boolean getBoolean(String key, Boolean defaultValue) {
        try {
            return Boolean.parseBoolean(getValue(key, defaultValue.toString()));
        } catch (Exception e) {
            LocationTrackerServer.getLogger().logError("Configuration: Failed to parse \"" + key + "\" with value \"" + getValue(key, "") + "\" to boolean");
        }

        return defaultValue;
    }
}
