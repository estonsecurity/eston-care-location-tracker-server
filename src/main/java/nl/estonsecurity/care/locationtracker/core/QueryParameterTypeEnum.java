package nl.estonsecurity.care.locationtracker.core;

public enum QueryParameterTypeEnum {
    INTEGER, FLOAT, BOOLEAN, STRING
}
