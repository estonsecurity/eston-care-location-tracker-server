package nl.estonsecurity.care.locationtracker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import nl.estonsecurity.care.locationtracker.core.Configuration;
import nl.estonsecurity.care.locationtracker.core.Database;
import nl.estonsecurity.care.locationtracker.core.Logger;
import nl.estonsecurity.care.locationtracker.websocket.Server;
import org.java_websocket.WebSocketImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LocationTrackerServer {

    private static boolean DEBUG = false;

    private static Logger logger;
    private static Configuration configuration;
    private static Database database;
    private static Server webSocketServer;
    private static nl.estonsecurity.care.locationtracker.http.Server httpServer;
    private static Gson gson;
    private static GoogleAuthenticator gAuth;

    // Source: http://patorjk.com/software/taag/#p=display&c=bash&f=Basic&t=E-TRACE
    private static final String logo = "\n" +
            "#                                                              \n" +
            "#  d88888b          d888888b d8888b.  .d8b.   .o88b. d88888b   \n" +
            "#  88'              `~~88~~' 88  `8D d8' `8b d8P  Y8 88'       \n" +
            "#  88ooooo             88    88oobY' 88ooo88 8P      88ooooo   \n" +
            "#  88~~~~~   C8888D    88    88`8b   88~~~88 8b      88~~~~~   \n" +
            "#  88.                 88    88 `88. 88   88 Y8b  d8 88.       \n" +
            "#  Y88888P             YP    88   YD YP   YP  `Y88P' Y88888P   \n" +
            "#                                                              \n" +
            "#  Location Tracker Server for E-Trace by Eston Security       \n" +
            "#  Development team: Jeroen Lafleur & Agus Nobel               \n" +
            "#                                                              \n";

    static {
        Thread shutDownHook = new Thread(LocationTrackerServer::dispose);
        shutDownHook.setPriority(10);

        Runtime.getRuntime().addShutdownHook(shutDownHook);
    }

    public static void main(String[] args) throws InterruptedException, IOException {

        System.out.println(Logger.ANSI_GREEN + logo + Logger.ANSI_RESET);

        // Initialize logger and load configuration file
        logger = new Logger();
        configuration = new Configuration("config.properties");
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        gAuth = new GoogleAuthenticator();

        // Set debug modes if debug is enabled
        if (configuration.getBoolean("debug", false)) {
            DEBUG = true;
            WebSocketImpl.DEBUG = true;
        }

        database = new Database(configuration);
        webSocketServer = new Server(configuration);
        httpServer = new nl.estonsecurity.care.locationtracker.http.Server(configuration);

        // Initialize SSL
        if (configuration.getBoolean("wss.enabled", false)) {
            webSocketServer.enableSecure(configuration);
        }

        // Start servers
        webSocketServer.start();
        httpServer.start();

        // Keep application running and watch for commands
        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String in = sysin.readLine();

            if (in.equals("exit")) {
                shutdown();
            }
        }
    }

    public static void shutdown() {
        System.exit(0);
    }

    public static void dispose() {
        logger.logShutdown("Stopping server...");

        try {
            if (webSocketServer != null) {
                webSocketServer.stop(1000);
            }

            if (httpServer != null) {
                httpServer.stop(0);
            }

            if (database != null) {
                database.close();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            logger.logShutdown("Stopped server");
        }
    }

    public static boolean isDebugEnabled() {
        return DEBUG;
    }

    public static Logger getLogger() {
        return logger;
    }

    public static Configuration getConfiguration() {
        return configuration;
    }

    public static Database getDatabase() {
        return database;
    }

    public static Server getWebSocketServer() {
        return webSocketServer;
    }

    public static nl.estonsecurity.care.locationtracker.http.Server getHttpServer() {
        return httpServer;
    }

    public static Gson getGson() {
        return gson;
    }

    public static GoogleAuthenticator getGAuth() {
        return gAuth;
    }
}
